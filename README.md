Extract, filter, and plot data from Italian Dipartimento della Protezione Civile [Official COVID-19 dataset](https://github.com/pcm-dpc/COVID-19).

## Why?
The dataset is provided as a single huge table. While this is not a problem for
the "national" dataset, which can be plotted easily using Excel; the "regional" 
dataset is much less understandable by Excel -- this is due to the fact that a
single table hold data from different series of data (which is grouped by region)

By using this tool one can not only extract the data for a single region and plot
it: one can also plot together data from different regions and compare them, in 
order to better contextualize the data itself.

## Licensing
This code is released under the MIT license. See [LICENSE](LICENSE) for details.
