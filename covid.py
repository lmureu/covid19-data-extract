import json
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import AutoMinorLocator


def initialize_plot(x_axis, y_axis, title):
    fig, ax = plt.subplots()
    ax.grid()
    ax.set(xlabel=x_axis, ylabel=y_axis, title=title)
    return fig, ax


def add_subplot(ax, dataset, x_axis, y_axis, label, where = lambda row: True):
    # Data for plotting
    d = [row for row in dataset if where(row)]
    t = [row[x_axis][:10] for row in d]
    s = [row[y_axis] for row in d]
    ax.semilogy(t, s, label=label)

def evenly_space_ticks(ticks, modulo, force_last = False):
    """
    Leaves `modulo - 1` invisible ticks between two adiacent visible ticks. If
    `force_last` is True, the last tick will always be visible (even if it
    shouldn't)

    e.g. if modulo is 4 the sequence is: visible, invisible, invisible,
    invisible, visible, ...
    """

    for index, tick in enumerate(ticks):
        tick.set_visible(index % modulo == 0)

    if force_last:
        ticks[-1].set_visible(True)


# Load dataset
dataset = json.load(open("dpc-covid19-ita-regioni.json"))

# Initialize plot
fig, ax = initialize_plot("data", "totale_casi", "@TODO: fix this")

# Add subplots
add_subplot(ax, dataset, "data", "totale_casi", "Piemonte", where = lambda el: el["denominazione_regione"] == "Piemonte")
add_subplot(ax, dataset, "data", "totale_casi", "Sardegna", where = lambda el: el["denominazione_regione"] == "Sardegna")

# Reformat plot
fig.legend(loc="right")
plt.setp(ax.get_xticklabels(), ha="right", rotation=30)
evenly_space_ticks(ax.get_xticklabels(), 5, True)

# fig.savefig("test.png")
plt.show()
